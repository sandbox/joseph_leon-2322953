CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Installation
 * Configuration
 * Directions
 * Similar Projects
 * Requirements
 * Maintainers


INTRODUCTION
------------
This module provides the ability to compare multiple products on an individual
product display. This functionality allows a user to look at different products
of a single product type and then choose which one they want. The compare
products are loaded in a colorbox. Through this module you can configure which
product displays are used and what fields to place in the colorbox.


FEATURES
--------
 * Compare multiple products on a single product display.
 * Customize the fields in the compare popup.
 * Front/Back image toggle functionality.
 * Choose product and auto update the individual product display product data.


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * This module generates a block with the compare button link in it. This block
   must be set to the specific product displays in order to function.
 * This module has a config screen at
   admin/config/development/commerce_compare_popup


DIRECTIONS
----------
 * Install and turn on the module.
 * Set the Compare Button block (created when module is turned on) to show on an
individual product display.
 * Go to the admin page admin/config/development/commerce_compare_popup.
 * Input the correct settings and save.
 * Test by clicking the compare button on the product display.


SIMILAR PROJECTS
----------------
Here is a similar module
(https://www.drupal.org/project/commerce_product_comparison). This other module
allows comparisons but on other pages. It doesn't provide a colorbox nor does it
have functional controls to click on a product and then get interaction on the
same product display page. For example, if we have an image gallery on a product
type and a product display has 3 products referenced we can compare all three
products in a colorbox then select one and it will close the colorbox and select
the appropriate product thereby generating the new image gallery.


REQUIREMENTS
------------
This module requires the following modules:
 * Commerce (https://www.drupal.org/project/commerce)
 * Colorbox (https://www.drupal.org/project/colorbox)


MAINTAINERS
-----------
Current maintainers:
 * Joseph Leon - https://www.drupal.org/u/joseph_leon
