<?php

/**
 * @file
 *
 * Provides an admin page for Commerce Compare configuration.
 */

/**
 * Implements hook_form().
 */
function commerce_compare_popup_form($form, &$form_state) {
  $content_types = db_query("SELECT type FROM {node_type}")->fetchCol();
  foreach ($content_types as $value) {
    $content_types_array[$value] = $value;
  }

  $form['form_instruction'] = array(
    '#type' => 'markup',
    '#markup' => t('<h2>Directions/Explanation</h2>
    <p>Here you can adjust settings so that a product display will have a button which will colorbox multiple products referenced to that product display. This could be used to display 3 different guitars in stock so the customer could choose which one they want or show three different colors of a shirt.</p>'),
  );
  // Return an empty array if nothing checked.
  $form['commerce_compare_popup_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content type selector'),
    '#options' => $content_types_array,
    '#default_value' => variable_get('commerce_compare_popup_content_types') ? variable_get('commerce_compare_popup_content_types') : array(),
    '#description' => t('Select which content types will use the compare feature.'),
    '#required' => TRUE,
    '#validated' => TRUE,
  );
  $product_reference_fields_li = commerce_compare_popup_get_reference_fields('li');
  $form['product_reference_fields'] = array(
    '#title' => t("Product refence fields"),
    '#prefix' => t('<div id="product_reference_fields">'),
    '#value' => 'The product reference fields are:<br /><ul>' . $product_reference_fields_li,
    '#suffix' => t('</ul></div>'),
    '#type' => 'fieldset',
  );
  $form['commerce_compare_popup_primary_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Primary image field'),
    '#default_value' => variable_get('commerce_compare_popup_primary_image'),
    '#description' => t('Input the image field for the primary/front image.'),
  );
  $form['commerce_compare_popup_single_or_frontback'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('commerce_compare_popup_single_or_frontback'),
    '#title' => t('Check if you want to show a secondary/back image in the popup.'),
  );
  // $form['place_for_back_image']['back_image'] = array(
  $form['commerce_compare_popup_back_image'] = array(
    '#type' => 'textfield',
    '#title' => t('Secondary image field'),
    '#default_value' => variable_get('commerce_compare_popup_back_image'),
    '#description' => t('Input the image field for the back image.'),
  );
  // Get the image styles.
  foreach (image_styles() as $style) {
    $styles[] = $style['name'];
  }
  foreach ($styles as $value) {
    $styles_select[$value] = $value;
  }
  $form['commerce_compare_popup_image_styles'] = array(
    '#type' => 'select',
    '#title' => t('Image style for Primary and Secondary images'),
    '#options' => $styles_select,
    '#default_value' => variable_get('commerce_compare_popup_image_styles'),
  );
  $form['commerce_compare_popup_add_fields'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('commerce_compare_popup_add_fields'),
    '#title' => t('Check to add more fields'),
  );
  $form['commerce_compare_popup_added_fields'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('commerce_compare_popup_added_fields'),
    '#title' => t('Extra fields'),
    '#description' => t('Add fields in the order you want them rendered under the image(s). Separate them by newlines.'),
  );
  $form['commerce_compare_popup_colorbox_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Colorbox height'),
    '#default_value' => variable_get('commerce_compare_popup_colorbox_height'),
    '#description' => t('This sets the height of the compare colorbox.'),
  );
  $form['commerce_compare_popup_colorbox_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Colorbox width'),
    '#default_value' => variable_get('commerce_compare_popup_colorbox_width'),
    '#description' => t('This sets the width of the compare colorbox.'),
  );

  return system_settings_form($form);
}
