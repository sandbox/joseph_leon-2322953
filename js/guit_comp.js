/**
 * @file
 *
 * A JavaScript file for Commerce Compare Popup.
 */

(function ($, Drupal, window, document, undefined) {

Drupal.behaviors.commerce_compare_popup = {
  attach: function(context, settings) {

    // Image front/back toggle.
    var toggleBtn    = $('.toggle-view-btn');
    var viewFrontBtn = $('#view-front-btn');
    var viewBackBtn  = $('#view-back-btn');
    var frontImages  = $('.front-images');
    var backImages   = $('.back-images');

    viewFrontBtn.click(function(event) {
      if(frontImages.is(':visible')) {
        backImages.addClass('hide');
      } else {
        backImages.addClass('hide');
        frontImages.removeClass('hide');
      }
    });
    viewBackBtn.click(function(event) {
      if(backImages.is(':visible')) {
        frontImages.addClass('hide');
      } else {
        frontImages.addClass('hide');
        backImages.removeClass('hide');
      }
    });

    // Popup choose button, select product, and close colorbox.
    var prodSelectId = $('#edit-selected-id');
    $('.choose').click(function(event) {
      $('#cboxOverlay').click();
      // Clear all selected attributes.
      prodSelectId.attr("selected",null);
      prodSelectId.removeAttr("selected");
      // Get value of the product.
      var value = $(this).attr("rel");
      // Change product on page.
      $('option[value="' + value + '"]').attr('selected','selected').parent().focus();
      $('option[value="' + value + '"]').click();
      $('option[value="' + value + '"]').change();
    });

    // Toggle back image field and extra field.
    var backImageAdmin    = $('.form-item-commerce-compare-popup-back-image');
    var addFieldsAdmin    = $('.form-item-add-fields');
    var singleOrFrontback = $('#edit-commerce-compare-popup-single-or-frontback');
    var addFields         = $("#edit-commerce-compare-popup-add-fields");

    if (singleOrFrontback.is(':checked')) {
      backImageAdmin.show();
    } else {
      backImageAdmin.hide();
    }
    singleOrFrontback.click(function() {
      if($(this).is(":checked")) {
        backImageAdmin.show();
      } else {
        backImageAdmin.hide();
      }
    });

    if (addFields.is(':checked')) {
      addFieldsAdmin.show();
    } else {
      addFieldsAdmin.hide();
    }
    addFields.click(function() {
      if($(this).is(":checked")) {
        addFieldsAdmin.show();
      } else {
        addFieldsAdmin.hide();
      }
    });

  }
};

})(jQuery, Drupal, this, this.document);
